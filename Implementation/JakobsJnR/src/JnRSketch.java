import processing.core.PApplet;
import JnR.Game;

public class JnRSketch extends PApplet 
{
	private static final long serialVersionUID = 5791736879123304697L;
	private Game _game;
	
	public void setup() 
	{ 
		_game = new Game(this);
	}

  	public void draw() 
  	{
  		_game.loop();
  	}
  	
  	public void keyPressed()
  	{
  		_game.keyPressed();
  	} 
  	
  	public void keyReleased()
  	{
  		_game.keyReleased();
  	}
}