package JnR;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import processing.core.PApplet;
import processing.core.PConstants;

public class Game 
{
	private List<Entity> _entities = new ArrayList<Entity>();
	private Player _player;
	private PApplet _applet;
	
	private float SPEED = 300;
	private float _x = 0;
	
	private boolean _keys[] = new boolean[3];
	
	private long _currentTime;
	private boolean _stop;
	
	public Game(PApplet applet)
	{
		_applet = applet;
		_applet.size(800, 600);
		
		restart();
	}
	
	private void createEntities()
	{
		loadMap("level1.lvl");
		
		_player = new Player(_applet, this, SPEED);
		_entities.add(_player);
	}

	private void loadMap(String file) 
	{
		try 
		{
			BufferedReader br = new BufferedReader(new FileReader(file));
						
			try 
			{
				String line;
				while((line = br.readLine()) != null)
				{
					if(line.startsWith("MovingBlock"))
					{
						loadMovingBlock(line);
					}
				}
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		} 
		catch (FileNotFoundException e) 
		{			
			e.printStackTrace();
		}
	}

	private void loadMovingBlock(String line) 
	{
		line = line.substring(13, line.length());
		String vals[] = line.split(",");

		float x = Float.parseFloat(vals[0].trim());
		float y = Float.parseFloat(vals[1].trim());
		float vX = Float.parseFloat(vals[2].trim());
		float vY = Float.parseFloat(vals[3].trim());
		float maxX = Float.parseFloat(vals[4].trim());
		float maxY = Float.parseFloat(vals[5].trim());
		
		MovingBlock bl = new MovingBlock(_applet, this, x, y, vX, vY, maxX, maxY);
		_entities.add(bl);
	}

	private void restart() {
		if(_x > 8000)
		{
			SPEED += 100;
		}
		else
		{
			SPEED = 300;
		}
		
		_keys[0] = _keys[1] = _keys[2] = false;
		
		_player = null;
		_entities.clear();
		createEntities();
		
		_x = 0;
  		_currentTime = System.currentTimeMillis();
		_stop = false;
	}
	
	public void loop()
	{
		_applet.background(255);
  		
  		long currentTime = System.currentTimeMillis();
  		long delta = currentTime - _currentTime;
  		_currentTime = currentTime;  	
  		
  		if(!_stop)
  		{
  			_x += SPEED * ((float)delta / 1000f);
  			
	  		for (Entity entity : _entities) {
				entity.move(delta);
			}
	  		
	  		for (Entity entity : _entities) {
				if(entity.intersect(_player) == true)
				{
					_stop = true;
					_player.hit();
				}
			}
	  		
	  		if(_stop == true && _x > 8000)
	  		{
	  			restart();
	  		}
  		}
  		else
  		{
  			_applet.fill(0);
  			_applet.text("Press space to restart", _applet.width/2 - 50, 50);
  		}
  		
  		for (Entity entity : _entities) {
			entity.render(delta);
		}
  		

			_applet.fill(0);
			_applet.text("Points: " + (int)_x, 20, 20);
	}
	
	private void movePlayer()
	{
		if(_keys[0])
		{
			_player.walk(-1);
		}
		else if(_keys[1])
		{
			_player.walk(1);			
		}
		else
		{
			_player.walk(0);
		}
	}

	public void keyPressed() {
		if(_stop)
		{
			if(_applet.key == ' ')
			{
				restart();
			}
		}
		else
		{
			if(_applet.keyCode == PConstants.LEFT)
			{
				_player.walk(-1);
				_keys[0] = true;
			}
			if(_applet.keyCode == PConstants.RIGHT)
			{
				_player.walk(1);
				_keys[1] = true;
			}
			if(_applet.keyCode == PConstants.UP && !_keys[2])
			{
				_keys[2] = true;
				_player.jump();
			}
		}
	}

	public void keyReleased() {
		if(_stop) return;
		
		if(_applet.keyCode == PConstants.LEFT)
		{
			_keys[0] = false;
		}
		if(_applet.keyCode == PConstants.RIGHT)
		{
			_keys[1] = false;
		}
		if(_applet.keyCode == PConstants.UP)
		{
			_keys[2] = false;
		}
		
		movePlayer();		
	}
	
	public float toViewPort(float x)
	{
		return x - _x;
	}
}