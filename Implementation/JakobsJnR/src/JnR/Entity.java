package JnR;

import java.awt.geom.Rectangle2D;

import processing.core.PApplet;

public class Entity 
{
	protected PApplet _applet;
	protected Game _game;
	protected float _x;
	protected float _y;
	
	public Entity(PApplet applet, Game game)
	{
		_applet = applet;
		_game = game;
	}
	
	public void move(long delta)
	{	}
	
	public void render(long delta)
	{	}
	
	public Rectangle2D getRect()
	{
		return new Rectangle2D.Float(_x, _y, 0, 0);
	}
	
	public boolean intersect(Entity e)
	{
		return false;
	}
}