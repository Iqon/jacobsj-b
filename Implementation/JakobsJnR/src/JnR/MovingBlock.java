package JnR;

import java.awt.geom.Rectangle2D;

import processing.core.PApplet;
import processing.core.PImage;

public class MovingBlock extends Entity 
{
	private float _vX;
	private float _vY;
	private float _offsetX;
	private float _offsetY;
	private float _maxX;
	private float _maxY;
	
	private Animation _block;
	
	public MovingBlock(PApplet applet, Game game, float x, float y, float vX, float vY, float maxX, float maxY) {
		super(applet, game);
		
		_x = x;
		_y = y;
		_offsetX = 0;
		_offsetY = 0;
		_vX = vX;
		_vY = vY;
		_maxX = maxX;
		_maxY = maxY;
		
		
		PImage[] imgs = new PImage[]
		{
				_applet.loadImage("Sprites/Block0.png"),
				_applet.loadImage("Sprites/Block1.png"),
				_applet.loadImage("Sprites/Block2.png"),
				_applet.loadImage("Sprites/Block3.png"),
				_applet.loadImage("Sprites/Block4.png"),
				_applet.loadImage("Sprites/Block3.png"),
				_applet.loadImage("Sprites/Block2.png"),
				_applet.loadImage("Sprites/Block1.png"),
		};
		
		for (PImage image : imgs) {
			image.resize(image.width, image.height);
		}
		
		_block = new Animation(_applet, 25, true, imgs);	
		_block.start();
	}

	public void render(long delta)
	{
		float y = _y + _offsetY + _applet.height - 50;	
		_block.render(delta, _game.toViewPort(_x + _offsetX), y);
	}

	public void move(long delta)
	{
		float deltaS = ((float) delta) / 1000f;
		
		_offsetX += _vX * deltaS;
		_offsetY += _vY * deltaS;
		
		if(Math.abs(_offsetX) >= _maxX)
		{
			_vX = -_vX;
		}	
		
		if(Math.abs(_offsetY) >= _maxY)
		{
			_vY = -_vY;
		}
	}
	
	public Rectangle2D getRect()
	{
		return new Rectangle2D.Float(_x + _offsetX, _y + _offsetY, 50, 50);
	}
	
	public boolean intersect(Entity e)
	{
		Rectangle2D rect = e.getRect();
		Rectangle2D self = getRect();
		
		if((rect.getX() + rect.getWidth()) > self.getX() &&
			rect.getX() < (self.getX() + self.getWidth()) &&
			(rect.getY() -  rect.getHeight()) < self.getY() && 
			(rect.getY()) > (self.getY() - self.getHeight()))
		{
			return true;
		}
		else
 		{
			return false;
		}
	}
	
	
}
