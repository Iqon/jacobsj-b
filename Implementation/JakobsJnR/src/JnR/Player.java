package JnR;

import java.awt.geom.Rectangle2D;

import processing.core.PApplet;
import processing.core.PImage;

public class Player extends Entity 
{		
	private final int SCALE = 7;
	private final int MAX_NO_JUMPS = 3;
	private final float JUMPSPEED = 800f;
	private final float SPEED = 500f;
	private final float GRAVITY = 2000f;
	private final float PLAYER_WIDHT = 70;
	private final float PLAYER_HEIGTH = 100;
	
	private float _viewPortSpeed;
	
	private int _jumpCounter = 0;
	private float _jumpSpeed;
	
	private int _direction = 0;
	
	private Animation _walkLeft;
	private Animation _walkRight;
	private Animation _standing;
	private Animation _jumpRightDown;
	private Animation _jumpRightUp;	
	private Animation _jumpLeftDown;
	private Animation _jumpLeftUp;	
	private Animation _hit;	
	private Animation _current;
	
	private boolean _downAnimRunning = false;
	
	public Player(PApplet applet, Game game, float viewPortSpeed) 
	{
		super(applet, game);
		
		_viewPortSpeed = viewPortSpeed;
		
		createAnimations();
		ChangeAnim();
		
		_x = _applet.width / 2 - PLAYER_WIDHT / 2;
	}	
	
	private void createAnimations()
	{
		createAnimWalkLeft();
		createAnimWalkRight();
		createAnimWalkStand();
		createAnimJumpRightUp();
		createAnimJumpRightDown();
		createAnimJumpLeftUp();
		createAnimJumpLeftDown();
		createAnimHit();
	}
	
	private void createAnimHit() {
		PImage[] imgs = new PImage[]
				{
				_applet.loadImage("Sprites/gethit.png"),
				};
		
		for (PImage image : imgs) {
			image.resize(image.width / SCALE, image.height / SCALE);
		}
		
		_hit = new Animation(_applet, 5000, true, imgs);		
	}

	private void createAnimJumpLeftDown() {
		PImage[] imgs = new PImage[]
				{
				_applet.loadImage("Sprites/jump_fall_left.png"),
				};
		
		for (PImage image : imgs) {
			image.resize(image.width / SCALE, image.height / SCALE);
		}
		
		_jumpLeftDown = new Animation(_applet, 5000, true, imgs);
	}

	private void createAnimJumpLeftUp() {
		PImage[] imgs = new PImage[]
				{
				_applet.loadImage("Sprites/jump_up_left.png"),
				};
		
		for (PImage image : imgs) {
			image.resize(image.width / SCALE, image.height / SCALE);
		}
		
		_jumpLeftUp = new Animation(_applet, 5000, true, imgs);		
	}
	
	private void createAnimJumpRightDown() {
		PImage[] imgs = new PImage[]
				{
				_applet.loadImage("Sprites/jump_fall.png"),
				};
		
		for (PImage image : imgs) {
			image.resize(image.width / SCALE, image.height / SCALE);
		}
		
		_jumpRightDown = new Animation(_applet, 5000, true, imgs);
	}

	private void createAnimJumpRightUp() {
		PImage[] imgs = new PImage[]
				{
				_applet.loadImage("Sprites/jump_up.png"),
				};
		
		for (PImage image : imgs) {
			image.resize(image.width / SCALE, image.height / SCALE);
		}
		
		_jumpRightUp = new Animation(_applet, 5000, true, imgs);		
	}

	private void createAnimWalkRight() {
		PImage[] imgs = new PImage[]
				{
				_applet.loadImage("Sprites/running_right (1).png"),
				_applet.loadImage("Sprites/running_right (2).png"),
				_applet.loadImage("Sprites/running_right (3).png"),
				_applet.loadImage("Sprites/running_right (4).png"),
				_applet.loadImage("Sprites/running_right (5).png"),
				_applet.loadImage("Sprites/running_right (6).png")
				};
		
		for (PImage image : imgs) {
			image.resize(image.width / SCALE, image.height / SCALE);
		}
		
		_walkRight = new Animation(_applet, 150, true, imgs);
	}

	private void createAnimWalkLeft() {
		PImage[] imgs = new PImage[]
				{
				_applet.loadImage("Sprites/running_left (1).png"),
				_applet.loadImage("Sprites/running_left (2).png"),
				_applet.loadImage("Sprites/running_left (3).png"),
				_applet.loadImage("Sprites/running_left (4).png"),
				_applet.loadImage("Sprites/running_left (5).png"),
				_applet.loadImage("Sprites/running_left (6).png")
				};
		
		for (PImage image : imgs) {
			image.resize(image.width / SCALE, image.height / SCALE);
		}
		
		_walkLeft = new Animation(_applet, 150, true, imgs);		
	}

	private void createAnimWalkStand() {
		PImage[] imgs = new PImage[]
				{
				_applet.loadImage("Sprites/standing (1).png"),
				_applet.loadImage("Sprites/standing (2).png")
				};
		
		for (PImage image : imgs) {
			image.resize(image.width / SCALE, image.height / SCALE);
		}
		
		_standing = new Animation(_applet, 150, true, imgs);
	}

	public void jump()
	{
		if(_jumpCounter < MAX_NO_JUMPS)
		{
			_jumpCounter ++;
			_jumpSpeed = JUMPSPEED;
			
			ChangeAnim();
		}		
	}
	
	public void walk(int dir)
	{
		if(_direction != dir)
		{
			_direction = dir;			
			ChangeAnim();
		}
	}
	
	public void render(long delta)
	{
		float y = _y + _applet.height - PLAYER_HEIGTH;		
		_current.render(delta, _game.toViewPort(_x), y);
	}
	
	public void move(long delta)
	{
		float deltaS = (float)delta / 1000f;

		if(_direction == 1)
		{
			_x += _direction * deltaS * (SPEED + _viewPortSpeed);
		}
		if(_direction == -1)
		{
			_x += _direction * deltaS * (SPEED - _viewPortSpeed);
		}
		
		if(_y > 0)
		{
			_jumpCounter = 0;
			_jumpSpeed = 0;
			_y = 0;			
			
			ChangeAnim();
		}
		else if(_y != 0)
		{
			_jumpSpeed -= GRAVITY * deltaS;
			
			if(_jumpSpeed < 0 && !_downAnimRunning)
			{
				ChangeAnim();
			}
		}
		
		_y -= _jumpSpeed * deltaS;		
	}
	
	private void ChangeAnim()
	{
		_downAnimRunning = false;

		if(_jumpCounter > 0)
		{
			if(_direction >= 0)
			{
				if(_jumpSpeed > 0)
				{
					_jumpRightUp.start();		
					_current = _jumpRightUp;		
				}
				else
				{					
					_jumpRightDown.start();	
					_current = _jumpRightDown;
					_downAnimRunning = true;
				}
			}
			else if(_direction == -1)
			{
				if(_jumpSpeed > 0)
				{
					_jumpLeftUp.start();
					_current = _jumpLeftUp;				
				}
				else
				{
					_jumpLeftDown.start();
					_current = _jumpLeftDown;	
					_downAnimRunning = true;
				}			
			}
		}
		else
		{
			if(_direction == 0)
			{
				_standing.start();
				_current = _standing;	
			}
			else if(_direction == 1)
			{
				_walkRight.start();	
				_current = _walkRight;			
			}
			else if(_direction == -1)
			{
				_walkLeft.start();
				_current = _walkLeft;				
			}
		}		
	}
	
	public Rectangle2D getRect()
	{
		return new Rectangle2D.Float(_x, _y, PLAYER_WIDHT - 20, PLAYER_HEIGTH - 20);
	}
	
	public boolean intersect(Entity e)
	{		
		float x = _game.toViewPort(_x);
		return (x < -PLAYER_WIDHT/2) || (x > _applet.width - PLAYER_WIDHT/2);
	}

	public void hit() {
		_hit.start();
		_current = _hit;
	}
}
