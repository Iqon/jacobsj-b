package JnR;

import processing.core.*;

public class Animation {
	private PApplet _parent;
	private boolean _repeat;
	private boolean _running;
	private PImage[] _images;
	
	private long _current;
	private long _delay;
	
	public Animation(PApplet parent, long delay, boolean repeat, PImage[] images)
	{
		_repeat = repeat;
		_parent = parent;
		_images = images;
		_delay = delay;
	}
	
	public void render(long delta, float x, float y)
	{
		if(_images != null && _running)
		{
			_current += delta;
		
			int no = ((int)(_current / _delay));
			
			if(no >= _images.length)
			{
				if(_repeat)
				{
					no %= _images.length;
				}
				else
				{
					stop();
					return;
				}
			}
			
			_parent.image(_images[no], x, y);
		}
	}
	
	public void start()
	{
		_running = true;	
		_current = 0;
	}
	
	public void stop()
	{
		_running = false;
	}
}
